# README #

Crud simples de status de CPF


### Versao do Docker Utilizada ###
Docker version 17.05.*
docker-compose version 1.16.*

### Instalacao e configuracao do container ###
git clone https://pedebodes@bitbucket.org/pedebodes/consulta_cpf.git

chown -R <usuario>.<diretorio>

docker-compose up

###  Acessando ###
Apos execucao do comento docker-compose up no terminal, acesse pelo navegador 
http://localhost:5000/ pagina Home ( contem informações de quantidade de acessos e CPF cadastratos e quantidade)
http://localhost:5000/consulta?cpf= -Consultar status de um determinado CPF

Toda a pagina navegavel, utilizando o framework Flask, e Bootstrap

### Dependencias ###
flask - Utilizada para facilitar o processo de requisicoes
redis - usado como contador de acesso
pymongo - usado para armazenar os cpf's e seus estatus.

### Who do I talk to? ###
* Gilberto I.Oliveira