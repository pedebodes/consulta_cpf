import json
import re
from flask import Flask, request, Response,render_template,redirect, url_for
from redis import Redis
from pymongo import MongoClient, errors


app = Flask(__name__)
redis = Redis(host='redis', port=6379)
teste_cpf = MongoClient('mongo', 27017).demo.teste_cpf

# home retorna dados do BD e quantidade de acesso
@app.route('/')
def home():
    count = redis.incr('hits')
    status = 'Consultas realizadas = {} \n'.format(count)
    retorno =list(teste_cpf.find({}))
    return render_template('home.html',retorno =retorno,status =status, qtd  = len(retorno) )


@app.route('/buscar')
def buscar():
  return render_template('consulta.html')

@app.route('/cadastro')
def cadastro():
  return render_template('cadastrar.html')

#consulta CPF
@app.route('/consulta', methods=['GET'])
def consulta():
    if request.method == 'GET':
        cpf = request.args.get('cpf')
        cpf = limpa(cpf)
        if cpf_valid(cpf):
            retorno = teste_cpf.find_one({'_id' : cpf})
            return Response(json.dumps(retorno), status=200, mimetype='application/json')
        else:
            return 'CPF Invalido'
    
#cadastrar CPF
@app.route('/cadastrar', methods = ['GET'])
def cadastrar():
    if request.method == 'GET':
        cpf = request.args.get('cpf')
        cpf = limpa(cpf)
        situacao = request.args.get('situacao')
    try:
      
        if cpf_valid(cpf):
            teste_cpf.insert_one({
                '_id': cpf,
                'status': situacao
            })
        else:
            return 'CPF Invalido'
        
    except errors.DuplicateKeyError as e:
        #Edita registro
        teste_cpf.update_one({'_id' : cpf}, {'$set': {'status':situacao} })   
    return redirect(url_for('home'))


#adicio a Black list
@app.route('/black', methods=['GET'])
def black():
    cpf = request.args.get('cpf')
    cpf = limpa(cpf)
    try:
      
        if cpf_valid(cpf):
            teste_cpf.insert_one({
                '_id': cpf,
                'status': 'BLOCK'
            })
        else:
            return 'CPF Invalido'
        
    except errors.DuplicateKeyError as e:
        #Edita registro
        teste_cpf.update_one({'_id' : cpf}, {'$set': {'status':'BLOCK'} })   

    return redirect(url_for('home'))
    


#adciona a White Lista
@app.route('/white', methods=['GET'])
def white():
    cpf = request.args.get('cpf')  
    cpf = limpa(cpf)
    try:
        if cpf_valid(cpf):
            teste_cpf.insert_one({
                '_id': cpf,
                'status': 'FREE'
            })
        else:
            return 'CPF Invalido'
       
    except errors.DuplicateKeyError as e:
        #Edita registro
        teste_cpf.update_one({'_id' : cpf}, {'$set': {'status':'FREE'} })
    return redirect(url_for('home'))

   
#valida  CPF
def cpf_valid(cpf, d1=0, d2=0, i=0):
    try:
        while i < 10:
            d1, d2, i = (d1 + (int(cpf[i]) * (11-i-1))) % 11 if i < 9 else d1, (d2 + (int(cpf[i]) * (11-i))) % 11, i + 1
        return (int(cpf[9]) == (11 - d1 if d1 > 1 else 0)) and (int(cpf[10]) == (11 - d2 if d2 > 1 else 0))
    except:
        return 0

#remove pontos do cpf
def limpa(v):
    v = v.replace(".","")
    v = v.replace("-","")
    v = v.replace(",","")    
    return v


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
