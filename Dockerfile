FROM python:3.4-alpine


ADD . /root
WORKDIR /root
RUN mkdir flask-mongodb
COPY ./ ./flask-mongodb/
RUN cat ./flask-mongodb/requirements.txt
RUN pip install -qr ./flask-mongodb/requirements.txt

ENTRYPOINT ["python", "./flask-mongodb/app.py"]
EXPOSE 5000

